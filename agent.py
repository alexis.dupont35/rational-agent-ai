# -*- coding: utf-8; mode: python -*-

# ENSICAEN
# École Nationale Supérieure d'Ingénieurs de Caen
# 6 Boulevard Maréchal Juin
# F-14050 Caen Cedex France
#
# Artificial Intelligence 2I1AE1

#
# @file agents.py
#
# @author Régis Clouard, Alexis Dupont, Benjamin Deschamps.
#

from __future__ import print_function
import random
import copy
import queue
import math
import sys
from tkinter.constants import TRUE, X
import utils

class Agent:
    """
    The base class for various flavors of the agent.
    This an implementation of the Strategy design pattern.
    """
    def init( self, gridSize ):
        raise Exception("Invalid Agent class, init() not implemented")

    def think( self, percept, isTraining = False ):
        raise Exception("Invalid Agent class, think() not implemented")

def pause( text):
    if sys.version_info.major >= 3:
        input(text)
    else:
        raw_input(text)

class DummyAgent( Agent ):
    """
    An example of simple Wumpus hunter brain: acts randomly...
    """
    isLearningAgent = False

    def init( self, gridSize ):
        pass

    def think( self, percept ):
        return random.choice(['shoot', 'grab', 'left', 'right', 'forward', 'forward'])

#######
####### Exercise: Rational Agent
#######

WALL='#'
UNKNOWN='?'
WUMPUSP='w'
WUMPUS='W'
PITP='p'
PIT='P'
WUMPUSPITP='x'
SAFE=' '
VISITED='.'
GOLD='G'

RIGHT  ='right'
LEFT = 'left'
FORWARD = 'forward'
CLIMB = 'climb'
SHOOT = 'shoot'
GRAB = 'grab'

DIRECTIONTABLE = [(0, -1), (1, 0), (0, 1), (-1, 0)] # North, East, South, West

class Node:
    x = 0
    y = 0
    cost = 0
    heuristic = 0
    parentx = 0
    parenty = 0 

class State():
    def __init__( self, gridSize ):
        self.size = gridSize
        self.worldmap = [[((y in [0, gridSize - 1] or  x in [0, gridSize - 1]) and WALL) or UNKNOWN
                          for x in range(gridSize) ] for y in range(gridSize)]
        self.direction = 1
        self.posx = 1
        self.posy = 1
        self.wumpusIsKilled = False
        self.goldIsGrabbed = False
        self.wumpusLocation = None
        self.arrowInventory = 1
        self.astarDone = False
        self.astarWumpusDone = False
        self.path = []
        self.index = 0

    def print_world( self ):
        """
        For debugging purpose.
        """
        for y in range(self.size):
            for x in range(self.size):
                print(self.get_cell(x, y) + " ", end=' ')
            print()

    def get_cell( self, x, y ):
        return self.worldmap[x][y]

    def set_cell( self, x, y, value ):
        self.worldmap[x][y] = value

    def get_cell_neighbors( self, x, y ):
        return [(x + dx, y + dy) for (dx,dy) in DIRECTIONTABLE]
    
    def get_forward_position( self, x, y, direction ):
        (dx, dy) = DIRECTIONTABLE[direction]
        return (x + dx, y + dy)

    def from_direction_to_action( self, direction ):
        if direction == self.direction:
            return FORWARD
        elif direction == (self.direction + 1) % 4:
            return RIGHT
        elif direction == (self.direction + 2) % 4:
            return RIGHT
        else:
            return LEFT

    def update_state_from_percepts( self, percept ):
        """
        Updates the current environment with regards to the percept information.
        """
        self.set_cell(self.posx, self.posy, VISITED)
        for neighbour in self.get_cell_neighbors(self.posx, self.posy):
            tile = self.get_cell(neighbour[0], neighbour[1])
            if tile == WALL or tile == VISITED or tile == SAFE:
                continue
            if percept.stench and percept.breeze:
                if tile == UNKNOWN or self.wumpusLocation == None:
                    self.set_cell(neighbour[0], neighbour[1], WUMPUSPITP)
                else:
                    self.set_cell(neighbour[0], neighbour[1], PITP)
            if percept.stench and not percept.breeze:
                if tile == UNKNOWN or tile == WUMPUSPITP:
                    if self.wumpusLocation == None:
                        self.set_cell(neighbour[0], neighbour[1], WUMPUSP)
                    else:
                        self.set_cell(neighbour[0], neighbour[1], SAFE)
                elif tile == PITP:
                    self.set_cell(neighbour[0], neighbour[1], SAFE)
            if not percept.stench and percept.breeze:
                if tile == UNKNOWN or tile == WUMPUSPITP:
                    self.set_cell(neighbour[0], neighbour[1], PITP)
                elif tile == WUMPUSP:
                    self.set_cell(neighbour[0], neighbour[1], SAFE)
            if not percept.stench and not percept.breeze:
                self.set_cell(neighbour[0], neighbour[1], SAFE)
        if percept.glitter:
            self.set_cell(self.posx, self.posy, GOLD)
        if percept.scream:
            self.set_cell(self.wumpusLocation[0], self.wumpusLocation[1], SAFE)
            self.wumpusIsKilled = True

        for x in range(self.size):
            for y in range(self.size):
                if self.get_cell(x, y) == VISITED:
                    wumpuspArray = []
                    pitArray = []
                    for neighbour in self.get_cell_neighbors(x, y):
                        if self.get_cell(neighbour[0], neighbour[1]) == WUMPUSP:
                            wumpuspArray.append(neighbour)
                    if len(wumpuspArray) == 1:
                        self.set_cell(wumpuspArray[0][0], wumpuspArray[0][1], WUMPUS)
                        self.wumpusLocation = wumpuspArray[0]
                        for i in range(self.size):
                            for j in range(self.size):
                                if self.get_cell(i, j) == WUMPUSP:
                                    self.set_cell(i, j, SAFE)
                                if self.get_cell(i, j) == WUMPUSPITP:
                                    self.set_cell(i, j, WUMPUSPITP)
                    for neighbour in self.get_cell_neighbors(x, y):
                        if self.get_cell(neighbour[0], neighbour[1]) == PITP or self.get_cell(neighbour[0], neighbour[1]) == PIT:
                            pitArray.append(neighbour)
                        
                    if len(pitArray) == 1:
                        self.set_cell(pitArray[0][0], pitArray[0][1], PIT)

class RationalAgent( Agent ):
    """
    Your smartest Wumpus hunter brain.
    """
    isLearningAgent = False

    def init( self, gridSize ):
        self.state = State(gridSize)
        self.state.set_cell(1,1, VISITED)

    def print_world( self ):
        self.state.print_world()

    def think( self, percept ):
        """
        Returns the best action regarding the current state of the game.
        Available actions are ['left', 'right', 'forward', 'shoot', 'grab', 'climb'].
        """
        self.state.update_state_from_percepts(percept)
        x = self.state.posx
        y = self.state.posy
        #To Grab the gold
        if self.state.get_cell(x,y) == GOLD and not self.state.goldIsGrabbed:
            self.state.goldIsGrabbed = True
            return GRAB

        #Perfoms Astar to find the path to (1,1)
        if self.state.goldIsGrabbed and self.state.wumpusIsKilled and not self.state.astarDone:
            startNode = Node()
            startNode.x = x
            startNode.y = y
            startNode.cost = 0
            startNode.heuristic = 0
            goalNode = Node()
            goalNode.x = 1
            goalNode.y = 1
            goalNode.cost = 0
            goalNode.heuristic = 0
            self.state.path = self.Astar(startNode, goalNode)
            self.state.astarDone = True
        
        #To climb away
        if x == 1 and y == 1 and self.state.goldIsGrabbed and self.state.wumpusIsKilled:
            return CLIMB

        #To go back to (1,1) at the end of the game
        if self.state.goldIsGrabbed and self.state.wumpusIsKilled and self.state.astarDone:
            tile = self.state.path[self.state.index]
            dx = tile.x - x
            dy = tile.y - y
            direction = 0
            for i in range(4):
                if(DIRECTIONTABLE[i] == (dx, dy)):
                    direction = i
            if self.state.from_direction_to_action(direction) != FORWARD:
                action = self.state.from_direction_to_action(direction)
                update_from_action(self.state, action)    
                return action
            else:
                self.state.index += 1
                action = self.state.from_direction_to_action(direction)
                update_from_action(self.state, action)    
                return action
            

        #Performs Astar to go to the wumpus.
        if  self.state.wumpusLocation != None and not self.state.wumpusIsKilled and not self.state.astarWumpusDone:
            startNode = Node()
            startNode.x = x
            startNode.y = y
            startNode.cost = 0
            startNode.heuristic = 0
            goalNode = Node()
            print(self.state.wumpusLocation[0])
            print(self.state.wumpusLocation[1])
            goalNode.x = self.state.wumpusLocation[0]
            goalNode.y = self.state.wumpusLocation[1]
            goalNode.cost = 0
            goalNode.heuristic = 0
            self.state.path = self.Astar(startNode, goalNode)
            self.state.astarWumpusDone = True

        #To go to the wumpus.
        if self.state.wumpusLocation != None and not self.state.wumpusIsKilled and self.state.astarWumpusDone:
            if(self.state.index >= len(self.state.path) - 2):
                self.state.index = 0
                self.state.path = []
            else:    
                tile = self.state.path[self.state.index]
                dx = tile.x - x
                dy = tile.y - y
                direction = 0
                for i in range(4):
                    if(DIRECTIONTABLE[i] == (dx, dy)):
                        direction = i
                if self.state.from_direction_to_action(direction) != FORWARD:
                    action = self.state.from_direction_to_action(direction)
                    update_from_action(self.state, action)    
                    return action
                else:
                    self.state.index += 1
                    action = self.state.from_direction_to_action(direction)
                    update_from_action(self.state, action)    
                    return action

        best_direction = -1
        best_score = -1000
        score = 0

        #Performs a random choice.
        if random.randint(0, 100) <= 20:
            random_action = [RIGHT, LEFT, FORWARD]
            action = random_action[random.randint(0, 2)]
            update_from_action(self.state, action)
            return action

        #Score computation
        for i in range(0,4):
            score = 0
            dx = DIRECTIONTABLE[i][0]
            dy = DIRECTIONTABLE[i][1]
            #kill the wumpus
            if self.state.get_cell(x + dx , y + dy) == WUMPUS:
                if self.state.from_direction_to_action(i) != FORWARD:
                    action = self.state.from_direction_to_action(i)
                    update_from_action(self.state, action)    
                    self.print_world()
                    return action
                else:
                    update_from_action(self.state, SHOOT)    
                    self.print_world()
                    self.state.wumpusIsKilled = True
                    return SHOOT
            else:

                if self.state.get_cell(x + dx , y + dy) == WUMPUS:
                    score += -1
                if self.state.get_cell(x + dx, y + dy ) == WUMPUSP:
                    score += -0.6
                if self.state.get_cell(x + dx , y + dy ) == WUMPUSPITP:
                    score += -0.6
                if self.state.get_cell(x + dx , y + dy ) == PIT:
                    score += -2
                if self.state.get_cell(x + dx , y + dy ) == PITP:
                    score += -0.5
                if self.state.get_cell(x + dx , y + dy ) == UNKNOWN:
                    score += 0.5
                if self.state.get_cell(x + dx , y + dy ) == SAFE:
                    score += 1
                if self.state.get_cell(x + dx , y + dy ) == WALL:
                    score += -0.5
                if self.state.get_cell(x + dx, y + dy ) == VISITED:
                    score += 0.1
                score += self.calculate_score(score, x, y, dx, dy, 0)
                print(score)
                if score > best_score:
                    best_score = score
                    best_direction = i
                elif score == best_score:
                    if self.state.from_direction_to_action(i) == FORWARD:
                        best_score = score
                        best_direction = i
        action = self.state.from_direction_to_action(best_direction)
        update_from_action(self.state, action)    
        self.print_world()
        return action


    def calculate_score(self, score, x, y, dx, dy, depth):
        unknown_count = 0
        depth += 1
        best_score = 0
        if depth == 2:
            return score
        for(ddx, ddy) in DIRECTIONTABLE:
            if (x + dx + ddx > self.state.size-1 or x + dx + ddx <0 or y + dy + ddy > self.state.size-1 or y + dy + ddy < 0):
                continue
            elif self.state.get_cell(x + dx + ddx, y + dy + ddy) == self.state.get_cell(x, y) :
                continue
            if self.state.get_cell(x + dx + ddx, y + dy + ddy) == UNKNOWN:
                unknown_count += 1
                if unknown_count == 3:
                    return score
        for i in range(0,4):
            test_score = score
            (ddx, ddy) = DIRECTIONTABLE[i]
            if (x + dx + ddx > self.state.size-1 or x + dx + ddx <0 or y + dy + ddy > self.state.size-1 or y + dy + ddy < 0):
                test_score += (1/depth) * self.calculate_score(test_score, x, y, dx, dy, depth)
            else:
                if self.state.get_cell(x + dx + ddx, y + dy + ddy) == WUMPUS:
                    test_score += -1
                if self.state.get_cell(x + dx + ddx, y + dy + ddy) == WUMPUSP:
                    test_score += -0.6
                if self.state.get_cell(x + dx + ddx, y + dy + ddy) == WUMPUSPITP:
                    test_score += -0.6
                if self.state.get_cell(x + dx + ddx, y + dy + ddy) == PIT:
                    test_score += -2
                if self.state.get_cell(x + dx + ddx, y + dy + ddy) == PITP:
                    test_score += -0.5
                if self.state.get_cell(x + dx + ddx, y + dy + ddy) == UNKNOWN:
                    test_score += 0.5
                if self.state.get_cell(x + dx + ddx, y + dy + ddy) == SAFE:
                    test_score += 1
                if self.state.get_cell(x + dx + ddx, y + dy + ddy) == WALL:
                    test_score += -0.5
                if self.state.get_cell(x + dx + ddx, y + dy + ddy) == VISITED:
                    test_score += 0.1
                test_score += ((1/depth) * self.calculate_score(test_score, dx, dy, ddx,ddy, depth))
            if test_score > best_score:
                best_score = test_score
        return best_score
    
    def Astar(self, start, goal):
        closedList = []
        openList = queue.PriorityQueue()
        openList.put(start)
        while not openList.empty() :
            tile = openList.get()
            
            
            if tile.x == goal.x and tile.y == goal.y:
                print(tile.parentx)
                print(tile.parenty)
                print("start")
                closedList.append(tile)
                return retrievePath(closedList, start.x, start.y, tile.parentx, tile.parenty, [tile])
            for (dx, dy) in DIRECTIONTABLE:
                cell = self.state.get_cell(tile.x+dx, tile.y+dy)
                if cell == VISITED or cell == WUMPUS or cell == SAFE:
                    neighbour = Node()
                    neighbour.x = tile.x + dx
                    neighbour.y = tile.y + dy
                    neighbour.cost = tile.cost + 1
                    neighbour.heuristic = distance(neighbour, goal)
                    neighbour.parentx = tile.x
                    neighbour.parenty = tile.y
                    (test, openList) = inWithLessCost(openList, neighbour)
                    if not (neighbour in closedList or test):
                        openList.put(neighbour)
            closedList.append(tile)

def update_from_action(self, action):
        if action == FORWARD:
            if not self.worldmap[self.get_forward_position(self.posx, self.posy, self.direction)[0]][self.get_forward_position(self.posx, self.posy, self.direction)[1]] == WALL :
                self.posx = self.get_forward_position(self.posx, self.posy, self.direction)[0]
                self.posy = self.get_forward_position(self.posx, self.posy, self.direction)[1]
        if action == LEFT:
            self.direction -= 1
            if self.direction == -1:
                self.direction = 3
        if action == RIGHT:
            self.direction += 1
            if self.direction == 4:
                self.direction = 0
        if action == GRAB:
            self.goldIsGrabbed = True
        if action == SHOOT and self.arrowInventory > 0:
            self.arrowInventory -= 1

def distance(tile, goal):
    return math.sqrt((goal.x - tile.x)**2 + (goal.y - tile.y)**2)

def inWithLessCost(openList, neighbour):
        cpyOpenList = queue.Queue(0)
        test = False
        while not openList.empty():
            tile = openList.get()
            tile = tile
            if tile.x == neighbour.x and tile.y == neighbour.y and tile.cost <= neighbour.cost:
                test=True
            cpyOpenList.put(tile)
        return (test, cpyOpenList)

def retrievePath(closedList, startx, starty, x, y, path):
        if(x == startx and y == starty):
            print(len(path))
            return path[::-1]
        test = False
        i = 0
        while not test :
            if closedList[i].x == x and closedList[i].y == y:
                path.append(closedList[i])
                test = True
            else :
                i += 1
        return retrievePath(closedList, startx, starty, closedList[i].parentx, closedList[i].parenty, path)