WUMPUS
======

I worked only on agent.py

# Goal :

    Kill the wumpus and grab the gold, then escape

# Exercice: Rational Agent

    python wumpus.py -a RationalAgent -w 7 -g 2
    # (_with a 10x10 world with random seed 2_)
